package com.appetitas.android.slidePuzzle.view;

import android.content.Context;
import android.graphics.Point;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.widget.GridView;
import com.appetitas.android.slidePuzzle.controller.ImageAdapter;
import com.appetitas.android.slidePuzzle.R;

public class PicDisplay extends GridView {

  private final Point clickDown = new Point(0,0);
  private final Point clickUp = new Point(0,0);
  private boolean move;
  private Context c;

  public PicDisplay(Context c) {
    super(c);
    this.c = c;
    Log.i(getResources().getString(R.string.app_name), "PicDisplay");
    this.setClickable(true);
    this.setFocusable(true);
  }

  @Override
  public boolean onKeyDown(int keyCode, KeyEvent event) {
    Log.i(getResources().getString(R.string.app_name), "onKeyDown");

    return super.onKeyDown(keyCode, event);
  }

  public boolean onTouchEvent(final MotionEvent e) {

    switch (e.getAction()) {
      case MotionEvent.ACTION_DOWN:
        //Log.i(getResources().getString(R.string.app_name), "ACTION_DOWN");
        clickDown.set((int)e.getX(), (int)e.getY());
        move = false;
        break;
      case MotionEvent.ACTION_MOVE:
        //Log.i(getResources().getString(R.string.app_name), "ACTION_MOVE");
        move = true;
        break;
      case MotionEvent.ACTION_UP:
        //Log.i(getResources().getString(R.string.app_name), "ACTION_UP");
        clickUp.set((int)e.getX(), (int)e.getY());
        if (move) {
          ((ImageAdapter) getAdapter()).swapImages(clickDown, clickUp);
        } else {
          ((ImageAdapter) getAdapter()).selectImage(clickUp);
        }
        ((ImageAdapter) getAdapter()).notifyDataSetChanged();
        break;
      case MotionEvent.ACTION_CANCEL:
        Log.i(getResources().getString(R.string.app_name), "ACTION_CANCEL");
        break;
      default:
        Log.i(getResources().getString(R.string.app_name), "DEFAULT");
    }
    return true;
  }


}
