package com.appetitas.android.slidePuzzle.model;

public class TimeModel {


  private long gameTime;
  private long moveTime;
  private int gameMoves;

  public void init(long time, int moves){
    this.gameTime = time;
    this.gameMoves = moves;
  }

  public void incMove() {
    gameMoves++;
  }

  public void incMoves(int moves) {
    gameMoves+=moves;
  }

  public void incTime(long time){
    gameTime+=time;
  }

  public long getGameTime() {
    return gameTime;
  }

  public int getGameMoves() {
    return gameMoves;
  }

  public void moveStart(){
    this.moveTime = System.currentTimeMillis();
  }

  public void moveStop(){
    this.gameTime+=(System.currentTimeMillis()-this.moveTime);
  }

}
