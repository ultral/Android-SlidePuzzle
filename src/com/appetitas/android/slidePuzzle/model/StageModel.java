package com.appetitas.android.slidePuzzle.model;

import android.graphics.drawable.BitmapDrawable;

public class StageModel {

  private int[] stageState;
  private BitmapDrawable[][] stage;
  private int size;
  private int picId;
  private int backId;

  public void setModel(BitmapDrawable[][] stage, int picId, int backId){
    this.size = stage.length;
    this.stage = stage;
    this.picId = picId;
    this.backId = backId;
    stageState = new int[size*size];

    for (int i = 0; i < size; i++)
      for (int j = 0; j < size; j++)
        stageState[i*size + j] = i*size + j;
  }

  public BitmapDrawable[][] getStage() {
    return stage;
  }

  private void swapModelStage(int first, int second){
    int tmp = stageState[first];
    stageState[first] = stageState[second];
    stageState[second] = tmp;
  }

  public void swapImages(int first, int second){
    BitmapDrawable tmp = stage[first % size][first / size];
    stage[first % size][first / size] = stage[second % size][second / size];
    stage[second % size][second / size] = tmp;

    swapModelStage(first, second);
  }

  public BitmapDrawable getBitmap(int place){
    return stage[place % size][place / size];
  }

  public int[] getStageState(){
    return stageState;
  }

  public void restoreStageState(int[] stageState){
    this.stageState = stageState;
    BitmapDrawable[][] tmpStage = new BitmapDrawable[size][size];

    for (int i = 0; i < size; i++)
      for (int j = 0; j < size; j++)
        tmpStage[i][j] = getBitmap(stageState[j*size + i]);

    stage = tmpStage;
  }

  public int getPicId() {
    return picId;
  }

  public int getBackId() {
    return backId;
  }

  public int getSize() {
    return size;
  }

  public boolean isFinish() {
    for (int i = 0; i < size*size; i++)
      if (stageState[i] != i) return false;

    return true;
  }
}
