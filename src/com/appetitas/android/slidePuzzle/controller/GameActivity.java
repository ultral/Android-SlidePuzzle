package com.appetitas.android.slidePuzzle.controller;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.appetitas.android.slidePuzzle.view.PicDisplay;
import com.appetitas.android.slidePuzzle.R;
import com.appetitas.android.slidePuzzle.model.StageModel;
import com.appetitas.android.slidePuzzle.model.TimeModel;

import java.lang.Override;import java.lang.String;import java.lang.SuppressWarnings;import java.lang.System;import java.util.Random;


public class GameActivity extends Activity {
  private static final Random rnd = new Random(System.currentTimeMillis());
  private static final int padding = 1;
  private static int size = 2;
  private static final int backPicNum = 4;
  private static final int picNum = 6;
  private final StageModel sm = new StageModel();
  private final TimeModel tm = new TimeModel();
  private PicDisplay pd;
  private int pic;
  private int back;
  private int movesNo;
  private long time;
  private int[] stageState;
  private static final String BACK = "background";
  private static final String SIZE = "size";
  private static final String GAME_TIME = "time";

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    Bundle extras = getIntent().getExtras();


    if (extras == null) {
      this.back = -1;
      size = 2;
    } else {
      this.back = extras.getInt(BACK);
      size = extras.getInt(SIZE);
    }

    this.pic = -1;
    this.time = 0;
    this.movesNo = 0;

    this.stageState = null;

    if (savedInstanceState != null) {
      back = savedInstanceState.getInt(BACK, -1);
      size = savedInstanceState.getInt(SIZE, 2);
      time = savedInstanceState.getLong(GAME_TIME, 0);
      movesNo = savedInstanceState.getInt("movesNo", 0);
      pic = savedInstanceState.getInt("pic", -1);
      if (savedInstanceState.containsKey("stageState"))
        stageState = savedInstanceState.getIntArray("stageState");
    }

    startGame(null);
  }

  private int getDrawable(int i) {
    if (i != -1) return i;

    i = rnd.nextInt(picNum);

    switch (i) {
      case 0:
        return R.drawable.o1;
      case 1:
        return R.drawable.o2;
      case 2:
        return R.drawable.o3;
      case 3:
        return R.drawable.o4;
      case 4:
        return R.drawable.o5;
      case 5:
        return R.drawable.o6;
      default:
        return R.drawable.o1;
    }
  }

  private int getBackDrawable(int i) {
    if (i != -1) return i;

    i = rnd.nextInt(backPicNum);

    switch (i) {
      case 0:
        return R.drawable.tlo1;
      case 1:
        return R.drawable.tlo2;
      case 2:
        return R.drawable.tlo3;
      case 3:
        return R.drawable.tlo4;
      default:
        return R.drawable.tlo1;
    }
  }

  private void init(int pic, int back, int movesNo, long time, int[] stageState) {

    int drawable = getDrawable(pic);
    int background = getBackDrawable(back);
    sm.setModel(PicCutter.cutPic(size, this, drawable, padding), drawable, background);
    tm.init(time, movesNo);
    ImageAdapter ia = new ImageAdapter(this, sm, stageState, padding, rnd, tm);
    pd = new PicDisplay(this);
    pd.setNumColumns(size);
    pd.setAdapter(ia);
    //this is how we make full screen
/*    this.requestWindowFeature(Window.FEATURE_NO_TITLE);
    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
    pd.setBackgroundResource(background);
  }

  public void finishGame(){
    Intent intent = new Intent();
    intent.putExtra(GAME_TIME, (int)tm.getGameTime()/1000);
    setResult(RESULT_OK, intent);
    finish();
  }

  @SuppressWarnings({"UnusedDeclaration"})
  public void startGame(View v) {
    init(pic, back, movesNo, time, stageState);
    tm.moveStart();
    setContentView(pd);
  }

  @Override
  public void onSaveInstanceState(Bundle outState) {
    outState.putInt(BACK, sm.getBackId());
    outState.putInt(SIZE, size);
    outState.putInt("pic", sm.getPicId());
    outState.putInt("movesNo", tm.getGameMoves());
    tm.moveStop();
    outState.putLong(GAME_TIME, tm.getGameTime());
    outState.putIntArray("stageState", sm.getStageState());
  }

  public void onPause() {
    super.onPause();
    tm.moveStop();
  }

  public void onResume() {
    super.onResume();
    tm.moveStart();
  }


}
