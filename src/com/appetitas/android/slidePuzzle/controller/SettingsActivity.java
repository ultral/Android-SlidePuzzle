package com.appetitas.android.slidePuzzle.controller;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import com.appetitas.android.slidePuzzle.R;

import java.util.Random;

public class SettingsActivity extends Activity {
  private static final Random rnd = new Random(System.currentTimeMillis());
  private static int size = 2;
  private static final int backPicNum = 4;
  private static int back;
  private static final String BACK = "background";
  private static final String SIZE = "size";
  private static String localizedBoardSize;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    Bundle extras = getIntent().getExtras();

    if (extras == null) {
      back = -1;
    } else {
      back = extras.getInt(BACK);
    }

    if (savedInstanceState != null)
      back = savedInstanceState.getInt("back", -1);

    localizedBoardSize = getResources().getString(R.string.board_size)+" ";

    makeMenu(back = getBackDrawable(back));
  }

  private void makeMenu(int background) {
    setContentView(R.layout.settings_menu);
    setSizeText();
    findViewById(R.id.settingsLayout).setBackgroundResource(background);
  }

  private int getBackDrawable(int i) {
    if (i != -1) return i;

    i = rnd.nextInt(backPicNum);

    switch (i) {
      case 0:
        return R.drawable.tlo1;
      case 1:
        return R.drawable.tlo2;
      case 2:
        return R.drawable.tlo3;
      case 3:
        return R.drawable.tlo4;
      default:
        return R.drawable.tlo1;
    }
  }

  private void setSizeText(){
    ((TextView) findViewById(R.id.counter)).setText(localizedBoardSize + size + "x" + size);
    setDifficultyText();
  }

  private void setDifficultyText(){
    String gameDifficulty = "";

    switch (size){
      case 2: gameDifficulty+="Very Easy"; break;
      case 3: gameDifficulty+="Easy"; break;
      case 4: gameDifficulty+="Medium"; break;
      case 5: gameDifficulty+="Hard"; break;
      case 6: gameDifficulty+="Very Hard"; break;
      case 7: gameDifficulty+="Challenging"; break;
      case 8: gameDifficulty+="Very Challenging"; break;
      case 9: gameDifficulty+="Almost Insane"; break;
      case 10: gameDifficulty+="Insane"; break;
      default: gameDifficulty+="Game Hacked!"; break;
    }


    ((TextView) findViewById(R.id.gameDifficulty)).setText(gameDifficulty);
  }

  @SuppressWarnings({"UnusedDeclaration"})
  public void removeOne(View v) {
    size = size > 2 ? size - 1 : size;
    setSizeText();
  }

  @SuppressWarnings({"UnusedDeclaration"})
  public void addOne(View v) {
    size = size < 10 ? size + 1 : size;
    setSizeText();
  }

  @SuppressWarnings({"UnusedDeclaration"})
  public void save(View v) {
    Intent intent = new Intent();
    intent.putExtra(SIZE, size);
    setResult(RESULT_OK, intent);
    finish();
  }

  @Override
  public void onSaveInstanceState(Bundle outState) {
    outState.putInt(BACK, back);
    outState.putInt(SIZE, size);
  }
}
