package com.appetitas.android.slidePuzzle.controller;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;

public class PicCutter {

  public static BitmapDrawable[][] cutPic(int size, Context c, int picId, int padding){
    int h = c.getResources().getDisplayMetrics().heightPixels-(padding*2*size);
    int w = c.getResources().getDisplayMetrics().widthPixels-(padding*2*size);

    Bitmap b = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(c.getResources(), picId), w, h, false);
    BitmapDrawable[][] bda = new BitmapDrawable[size][size];

    int heightChunk = h/size;
    int widthChunk = w/size;

    for (int i = 0; i < size; i++)
      for (int j = 0; j < size; j++){
       bda[i][j] = new BitmapDrawable(Bitmap.createBitmap(b, i*widthChunk, j*heightChunk, widthChunk, heightChunk));
      }

    return bda;
  }

}
