package com.appetitas.android.slidePuzzle.controller;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.appetitas.android.slidePuzzle.R;

import java.util.Set;

public class HighScoreActivity extends Activity {
  private static int back;
  private static final String BACK = "background";
  private static final String BEST_SCORE = "bestScore";
  private static final int MAX_GAME_SIZE = 9;
  private static final int GAME_OFFSET = 2;
  private static SharedPreferences myPrefs;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    Bundle extras = getIntent().getExtras();

    if (extras == null) {
      back = -1;
    } else {
      back = extras.getInt(BACK);
    }

    myPrefs = getSharedPreferences("myPrefs", MODE_PRIVATE);

    if (savedInstanceState != null)
      back = savedInstanceState.getInt(BACK);

    makeMenu(back);
    fillScores();
  }

  private void fillScores() {
    final String[] scores = new String[MAX_GAME_SIZE];
    int time;
    String timeStr;
    for (int i = 0; i < MAX_GAME_SIZE; i++) {
      time = myPrefs.getInt(BEST_SCORE + i, -1);
      if (time == -1) {
        timeStr = " ???";
      } else {
        timeStr = time + " sec";
      }
      scores[i] = "Size " + (i + GAME_OFFSET) + "x" + (i + GAME_OFFSET) + " best time:" + timeStr;
    }
    setText(scores);
  }

  @SuppressWarnings({"UnusedDeclaration"})
  public void resetScore(View v) {
    SharedPreferences.Editor e = myPrefs.edit();
    for (int i = 0; i < MAX_GAME_SIZE; i++) {
      e.putInt(BEST_SCORE + i, -1);
    }
    e.commit();
    fillScores();
  }

  private void setText(String[] scores) {

    String text = "";

    for (int i = 0; i < MAX_GAME_SIZE; i++) {
      text += scores[i] + System.getProperty("line.separator");
    }

    ((TextView) findViewById(R.id.highScoresText)).setText(text);
  }

  private void makeMenu(int background) {
    setContentView(R.layout.high_scores_menu);
    findViewById(R.id.highScoresLayout).setBackgroundResource(background);
  }

  @Override
  public void onSaveInstanceState(Bundle outState) {
    outState.putInt(BACK, back);
  }
}
