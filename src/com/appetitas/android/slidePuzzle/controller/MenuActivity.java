package com.appetitas.android.slidePuzzle.controller;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import com.appetitas.android.slidePuzzle.R;

import java.util.Random;

public class MenuActivity extends Activity {
  private static final Random rnd = new Random(System.currentTimeMillis());
  private static int size = 2;
  private static final int backPicNum = 4;
  private static int back;
  private static final int NEW_GAME = 1;
  private static final String BACK = "background";
  private static final String SIZE = "size";
  private static final int SETTINGS = 2;
  private static final int HIGH_SCORES = 3;
  private static final String GAME_TIME = "time";
  private static final String BEST_SCORE = "bestScore";
  private static SharedPreferences myPrefs;
  private static final int MAX_GAME_SIZE = 9;
  private static final int GAME_OFFSET = 2;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    back = -1;

    if (savedInstanceState != null)
      back = savedInstanceState.getInt(BACK, -1);

    myPrefs = getSharedPreferences("myPrefs", MODE_PRIVATE);
    makeMenu(back = getBackDrawable(back));
  }

  private void makeMenu(int background) {
    setContentView(R.layout.menu);
    findViewById(R.id.menuLayout).setBackgroundResource(background);
  }

  private int getBackDrawable(int i) {
    if (i != -1) return i;

    i = rnd.nextInt(backPicNum);

    switch (i) {
      case 0:
        return R.drawable.tlo1;
      case 1:
        return R.drawable.tlo2;
      case 2:
        return R.drawable.tlo3;
      case 3:
        return R.drawable.tlo4;
      default:
        return R.drawable.tlo1;
    }
  }

  @SuppressWarnings({"UnusedDeclaration"})
  public void startGame(View v) {
    Intent intent = new Intent(this, GameActivity.class);
    intent.putExtra(BACK, back);
    intent.putExtra(SIZE, size);
    startActivityForResult(intent, NEW_GAME);
  }

  @SuppressWarnings({"UnusedDeclaration"})
  public void settings(View v) {
    Intent intent = new Intent(this, SettingsActivity.class);
    intent.putExtra(BACK, back);
    startActivityForResult(intent, SETTINGS);
  }

  @SuppressWarnings({"UnusedDeclaration"})
  public void highScore(View v) {
    Intent intent = new Intent(this, HighScoreActivity.class);
    intent.putExtra(BACK, back);
    startActivity(intent);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    if (resultCode == RESULT_OK) {
      switch (requestCode) {
        case NEW_GAME:
          int gameTime = data.getExtras().getInt(GAME_TIME);
          int bestTime = myPrefs.getInt(BEST_SCORE + (size-GAME_OFFSET), Integer.MAX_VALUE);
          if (bestTime == -1 || bestTime > gameTime) {
            SharedPreferences.Editor e = myPrefs.edit();
            e.putInt(BEST_SCORE + (size-GAME_OFFSET), gameTime); // add or overwrite someValue
            e.commit();
          }
          break;
        case SETTINGS:
          size = data.getExtras().getInt(SIZE);
          break;
        case HIGH_SCORES:
          break;
      }
    }
  }

  @Override
  public void onSaveInstanceState(Bundle outState) {
    outState.putInt("back", back);
  }
}
