package com.appetitas.android.slidePuzzle.controller;

import android.content.Context;
import android.graphics.Point;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;
import com.appetitas.android.slidePuzzle.controller.GameActivity;
import com.appetitas.android.slidePuzzle.model.StageModel;
import com.appetitas.android.slidePuzzle.model.TimeModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ImageAdapter extends BaseAdapter {

  private StageModel stageModel;
  private TimeModel timeModel;
  private ImageView[][] iva;
  private int h;
  private int w;
  private int invisible = 0;
  private int alpha = -1;
  private int size;
  private Context c;
  private boolean win;
  private boolean start;


  public ImageAdapter(Context c, StageModel stageModel, int[] stageState, int padding, Random rnd, TimeModel tm) {
    this.stageModel = stageModel;
    this.timeModel = tm;
    this.size = stageModel.getSize();
    h = c.getResources().getDisplayMetrics().heightPixels / size;
    w = c.getResources().getDisplayMetrics().widthPixels / size;
    this.c = c;

    win = false;
    start = false;

    iva = new ImageView[size][size];

    ImageView imageView;

    for (int i = 0; i < size; i++)
      for (int j = 0; j < size; j++) {
        imageView = new ImageView(c);
        imageView.setLayoutParams(new GridView.LayoutParams(w, h));
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setPadding(padding, padding, padding, padding);
        iva[i][j] = imageView;
      }


    if (stageState == null) {
      for (int i = 0; i < size * size * 10; i++)
        swapImages(invisible, genMove(rnd));

      if (stageModel.isFinish())
        swapImages(invisible, genMove(rnd));
    } else {
      stageModel.restoreStageState(stageState);

      for (int i = 0; i < size * size; i++)
        if (stageState[i] == 0) invisible = i;
    }

    start = true;
    tm.moveStart();
  }

  private int genMove(Random rnd) {
    List<Integer> l = new ArrayList<Integer>(4);

    if (invisible - 1 >= 0 && (invisible % size > (invisible - 1) % size)) l.add(invisible - 1);
    if ((invisible + 1) < size * size && (invisible % size < (invisible + 1) % size)) l.add(invisible + 1);
    if (invisible - size >= 0) l.add(invisible - size);
    if (invisible + size < size * size) l.add(invisible + size);

    return l.get(rnd.nextInt(l.size()));
  }

  public int getCount() {
    return size * size;
  }

  public Object getItem(int position) {
    return null;
  }

  public long getItemId(int position) {
    return 0;
  }

  // create a new ImageView for each item referenced by the Adapter
  public View getView(int position, View convertView, ViewGroup parent) {
    ImageView imageView = iva[position % size][position / size];

    if (position == invisible && !win) {
      imageView.setVisibility(View.INVISIBLE);
    } else {
      imageView.setVisibility(View.VISIBLE);
    }

    if (position == alpha && !win) {
      imageView.setAlpha(128);
    } else {
      imageView.setAlpha(255);
    }

    imageView.setImageDrawable(stageModel.getBitmap(position));
    return imageView;
  }

  private int getPicNum(final Point p) {
    return (p.x / w) + (size * (p.y / h));
  }

  public void swapImages(final Point first, final Point second) {
    swapImages(getPicNum(first), getPicNum(second));
  }

  private void swapImages(int i, int j) {
    if (win) {
      ((GameActivity) c).finishGame();
      return;
    }

    if (invisible != i && invisible != j)
      return;

    if (i != j + 1 && j != i + 1 && i != j - size && i != j + size && j != i - size && j != i + size)
      return;

    if ((i == j + 1 && (i % size < j % size)) || (j == i + 1 && (j % size < i % size)))
      return;

    stageModel.swapImages(i, j);

    if (start)
      timeModel.incMove();

    if (invisible == i) {
      invisible = j;
    } else {
      invisible = i;
    }

    alpha = -1;

    if (stageModel.isFinish() && start) {
      timeModel.moveStop();
      Toast.makeText(c, "WYGRANA! Gra trwała "+(int)timeModel.getGameTime()/1000+" sekund i "+timeModel.getGameMoves()+" ruchów.", Toast.LENGTH_LONG).show();
      win = true;
    }

  }

  public void selectImage(final Point p) {
    if (win) {
      ((GameActivity) c).finishGame();
      return;
    }
    int i = getPicNum(p);
    if (alpha == -1) {
      alpha = i;
    } else if (alpha == i) {
      alpha = -1;
    } else {
      swapImages(alpha, i);
    }
  }

}
